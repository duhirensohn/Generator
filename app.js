const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const fs = require('fs');
const translator = require('./assets/helpers/translator');
const compression = require('compression');
const helmet = require('helmet');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(function (req, res, next) {
    req.locale = 'en';
    req.t = translator;
    res.locals.t = translator;
    next();
});

app.use(compression());
app.use(helmet());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.locals.asset = require('./assets/helpers/asset');
app.locals.flag = require('./assets/helpers/flag');
app.locals.includedynamic = require('./assets/helpers/includedynamic');
app.locals.httpMethodColor = require('./assets/helpers/httpMethodColor');

app.use(`/`, require('./routes/index'));
app.use('/api', require('./routes/api'));

app.use(function(req, res, next) {
    next(createError(404));
});

app.use(function(err, req, res, next) {
    if (req.app.get('env') !== 'development') {
        delete err.stack;
    }
    res.locals.error = err;

    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
